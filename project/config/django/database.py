# coding=utf-8
from __future__ import unicode_literals

class DevelopmentDatabaseSettings(object):
    @property
    def DATABASES(self):
        databases = {
            'default': {
                'ENGINE': 'django.db.backends.postgresql_psycopg2',
                # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
                'NAME': 'albums',  # Or path to database file if using sqlite3.
                'USER': 'postgres',  # Not used with sqlite3.
                'PASSWORD': 'postgres',  # Not used with sqlite3.
                'HOST': '127.0.0.1',  # Set to empty string for localhost. Not used with sqlite3.
                'PORT': 5432,  # Set to empty string for default. Not used with sqlite3.
            },
        }
        return databases


class StagingDatabaseSettings(object):
    @property
    def DATABASES(self):
        databases = {}
        databases['default'] = {}
        return databases


class ProductionDatabaseSettings(object):
    @property
    def DATABASES(self):
        databases = {}
        databases['default'] = {}
        import dj_database_url
        db_from_env = dj_database_url.config()
        databases['default'].update(db_from_env)
        return databases
