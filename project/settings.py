# coding=utf-8
from __future__ import unicode_literals

import sys
import os
from configurations import Settings

from config.django.database import DevelopmentDatabaseSettings, StagingDatabaseSettings, ProductionDatabaseSettings
from config.django.email import DevelopmentEmailSettings, StagingEmailSettings, ProductionEmailSettings
from config.django.i18n import LocaleSettings
from config.django.media import DevelopmentMediaSettings, StagingMediaSettings, ProductionMediaSettings
from config.django.middleware import MiddlewareSetings
from config.django.logging import LoggingSettings
from config.django.template import TemplateSettings

from config.apps.restframework import RestFrameworkSettings


class BaseSettings(LocaleSettings, LoggingSettings, MiddlewareSetings, TemplateSettings,
                   RestFrameworkSettings, Settings):
    PROJECT_ROOT = os.path.realpath(os.path.dirname(__file__))
    sys.path.insert(0, os.path.join(PROJECT_ROOT, 'apps'))

    ADMINS = MANAGERS = []

    ALLOWED_HOSTS = []

    SITE_ID = 1

    SECRET_KEY = 'dfsfsdfsfetreyhg454te'
    # from django.utils.crypto import get_random_string
    # get_random_string(50, 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)')

    ROOT_URLCONF = 'project.urls'

    WSGI_APPLICATION = 'project.wsgi.application'

    INSTALLED_APPS = (
        'admin_tools',
        'admin_tools.theming',
        'admin_tools.menu',
        'admin_tools.dashboard',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.sites',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'django.contrib.admin',

        'gunicorn',
        #'raven.contrib.django.raven_compat',
        'django',
        'rest_framework',

        'home',
        'albums',

    )


class Development(DevelopmentEmailSettings, DevelopmentDatabaseSettings, DevelopmentMediaSettings,
                  BaseSettings):
    DEBUG = TEMPLATE_DEBUG = True


class Staging(StagingEmailSettings, StagingDatabaseSettings, StagingMediaSettings,
              BaseSettings):
    pass


class Production(ProductionEmailSettings, ProductionDatabaseSettings, ProductionMediaSettings,
                 BaseSettings):
    pass
