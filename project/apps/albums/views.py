from rest_framework import generics, viewsets
from rest_framework.filters import OrderingFilter
from .serializers import AlbumSerializer
from .models import Album


class AlbumListView(generics.ListAPIView):
    queryset = Album.objects.all()
    serializer_class = AlbumSerializer
    paginate_by = 10
    filter_backends = (OrderingFilter,)
    ordering_fields = '__all__'

    #def get(self, request, *args, **kwargs):
    #    return super(AlbumListView, self).get(request, *args, **kwargs)

    def get_queryset(self):
        #if 'order' in self.request.query_params and self.request.query_params['order'] in AlbumSerializer.Meta.fields:
        #    order = self.request.query_params['order']
        #    return Album.objects.all().order_by(self.request.query_params['order'])
        return Album.objects.all()
