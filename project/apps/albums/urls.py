from django.conf.urls import url, include
from .views import AlbumListView


urlpatterns = [
    url(r'^albums/', AlbumListView.as_view(), name='album-list'),


]
