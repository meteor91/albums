from __future__ import unicode_literals

from rest_framework import serializers

from .models import Album


class AlbumSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(read_only=True)
    name = serializers.CharField(max_length=255)
    year = serializers.IntegerField()
    performer = serializers.StringRelatedField()

    class Meta:
        model = Album
        fields = ('pk', 'name', 'year', 'performer')


