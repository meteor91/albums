# coding=utf-8
from __future__ import unicode_literals

from django.db import models


class Performer(models.Model):
    name = models.CharField('Название', max_length=255)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = 'Исполнитель'
        verbose_name_plural = 'Исполнители'


class Album(models.Model):
    name = models.CharField('Название', max_length=255)
    year = models.PositiveIntegerField('Год выпуска')
    performer = models.ForeignKey(Performer, verbose_name='Исполнитель')

    class Meta:
        ordering = ('year', )
        verbose_name = 'Альбом'
        verbose_name_plural = 'Альбомы'