from django.contrib import admin

from .models import Album, Performer


@admin.register(Performer)
class PerformerAdmin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(Album)
class AlbumAdmin(admin.ModelAdmin):
    list_display = ('name', 'year', 'performer')