# coding=utf-8
from __future__ import unicode_literals



#  Based on: http://www.djangosnippets.org/snippets/73/
#
#  Modified by Sean Reifschneider to be smarter about surrounding page
#  link context.  For usage documentation see:
#
#     http://www.tummy.com/Community/Articles/django-pagination/


def paginate_tag(context, adjacent_pages=2):
    """
    To be used in conjunction with the object_list generic view.

    Adds pagination context variables for use in displaying first, adjacent and
    last page links in addition to those created by the object_list generic
    view.

    """
    page_obj = context['page_obj']
    paginator = context['paginator']

    page = page_obj.number
    last_page = paginator.num_pages

    start_page = max(page - adjacent_pages, 1)
    if start_page <= 3:
        start_page = 1

    end_page = page + adjacent_pages + 1
    if end_page >= last_page - 1:
        end_page = last_page + 1

    page_numbers = filter(lambda n: 0 < n <= last_page, range(start_page, end_page))

    return {
        'page_obj': page_obj,
        'paginator': paginator,
        # 'hits': context['hits'],
        'results_per_page': paginator.per_page,
        'page': page,
        'pages': last_page,
        'page_numbers': page_numbers,
        'next': page - 1 if page != 1 else 1,
        'previous': page + 1 if page != last_page else last_page,
        'has_next': page_obj.has_next(),
        'has_previous': page_obj.has_previous(),
        'show_first': 1 not in page_numbers,
        'show_last': last_page not in page_numbers,
    }
