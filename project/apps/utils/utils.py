# coding=utf-8
from __future__ import unicode_literals

import os
import uuid
import hashlib
from pytils import translit


# ************************************************  UPLOADING IMAGES  ************************************************ #
DEFAULT_PATH = 'upload'
MAX_FILENAME_LENGTH = 100


def create_file_path(*args):
    def translify_upload_name(name):
        return '.'.join([translit.slugify(s) for s in name.rsplit('.', 1)])

    path_elements = map(lambda x: translify_upload_name(unicode(x)), args)

    name_splitted = path_elements[-1].rsplit('.', 1)

    new_file_name = unicode(uuid.uuid4())

    path_elements[-1] = '.'.join((new_file_name, name_splitted[1]))

    path = os.path.join(DEFAULT_PATH)
    for path_element in path_elements:
        path = os.path.join(path, path_element)
    return path


def create_image_path(*args, **kwargs):
    def translify_upload_name(name):
        return u'.'.join([translit.slugify(s) for s in name.rsplit(u".", 1)])

    with_hash = kwargs.get(u'hash', False)

    shorten = kwargs.get(u'shorten', False)

    path_elements = map(unicode, args)

    name_splitted = path_elements[-1].rsplit(u'.', 1)
    new_file_name = name_splitted[0]
    file_name_length = MAX_FILENAME_LENGTH - (len(name_splitted[1]) + 1)
    if shorten:
        # shorten the file name (which is supposed to be the last element of the list)
        new_file_name = new_file_name[:file_name_length]

    if with_hash:
        # get hash of the file name so that it could not be cut by adblock for suspicious name
        new_file_name = hashlib.md5(new_file_name).hexdigest()[:file_name_length]

    path_elements[-1] = u'.'.join((new_file_name, name_splitted[1]))

    path = os.path.join(DEFAULT_PATH)
    for path_element in path_elements:
        path = os.path.join(path, translify_upload_name(path_element))
    return path


def append_to_dict(dictionary, key, value):
    try:
        dictionary[key].append(value)
    except AttributeError:
        # object has no attribute 'append'
        dictionary[key] = [dictionary[key], value]
    except KeyError:
        dictionary[key] = [value, ]
