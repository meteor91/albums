# coding=utf-8
from __future__ import unicode_literals

from modeltranslation.translator import TranslationOptions, translator


# ****************************************  REGISTERING MODEL FOR TRANSLATION  *************************************** #
def register_model_for_translation(model, fields_for_translation):
    class ModelTranslationOptions(TranslationOptions):
        fields = fields_for_translation

    translator.register(model, ModelTranslationOptions)


def models_for_translation(*args):
    """
    Arguments must be a tuple (Model, (field_to_translate_1, ...))
    """
    for entry in args:
        register_model_for_translation(entry[0], entry[1])
