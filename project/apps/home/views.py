from django.contrib.auth.models import User
from rest_framework import viewsets, generics
from .serializers import UserSerializer
from django.views.generic import TemplateView


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class IndexView(TemplateView):
    template_name = 'home/index.html'