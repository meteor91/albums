var AppDispatcher = require('../dispatchers/AppDispatcher');
var assign = require('object-assign');
var EventEmitter = require('events').EventEmitter;


var CHANGE_EVENT = 'change';

var _store = {
    counter: 0
};

function _increaseCounter() {
    _store.counter+=1;
}

function _decreaseCounter() {
    _store.counter-=1;
}


var AppStore = assign({}, EventEmitter.prototype, {
    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

	addChangeListener: function(callback) {
		this.on(CHANGE_EVENT, callback);
	},

	removeChangeListener: function(callback) {
		this.removeListener(CHANGE_EVENT, callback);
	},

    getCounterVal: function() {
        return _store.counter;
    },

    dispatcherIndex: AppDispatcher.register(function(payload) {
        var action = payload.action;

        switch(action.actionType) {
            case 'COUNTER_INCREASE':
                _increaseCounter();
                break;
            case 'COUNTER_DECREASE':
                _decreaseCounter();
                break;
        }

        AppStore.emitChange();

        return true;
    })

});

module.exports = AppStore;