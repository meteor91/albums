var AppDispatcher = require('../dispatchers/AppDispatcher');
var assign = require('object-assign');
var EventEmitter = require('events').EventEmitter;

var request = require('superagent');

var CHANGE_EVENT = 'change';

var _albums = [];



function _addAlbum() {

}

function _orderQuery(order) {
    var fields = [];
    fields.push(order.name=='asc' ? 'name' : '-name');
    fields.push(order.year=='asc' ? 'year' : '-year');
    fields.push(order.performer=='asc' ? 'performer' : '-performer');

    return 'ordering='+fields.join(',');
}

var AlbumsStore = assign({}, EventEmitter.prototype, {
    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

	addChangeListener: function(callback) {
		this.on(CHANGE_EVENT, callback);
	},

	removeChangeListener: function(callback) {
		this.removeListener(CHANGE_EVENT, callback);
	},

    getAlbums: function() {
        return _albums;
    },

    dispatcherIndex: AppDispatcher.register(function(payload) {
        var action = payload.action;

        switch(action.actionType) {
            case 'SHOW_ALBUMS_LIST':
                var order = action.order;

                request
                    .get('/albums/?format=json&ordering='+order)
                    .end(function (err, res) {
                        _albums = [];
                        res.body.forEach(function (item) {
                            _albums.push(item);
                        });
                        console.log(_albums);
                        AlbumsStore.emitChange();
                    });

                break;
            case 'ALBUMS_ADD':
                _addAlbum();
                AlbumsStore.emitChange();
        }



        return true;
    })
});

module.exports = AlbumsStore;