var AppDispatcher = require('../dispatchers/AppDispatcher');

var AppActions = {
    increaseCounter: function() {
        AppDispatcher.handleViewAction({
            actionType: 'COUNTER_INCREASE'
        })
    },

    decreaseCounter: function() {
        AppDispatcher.handleViewAction({
            actionType: 'COUNTER_DECREASE'
        })
    }
};


module.exports = AppActions;