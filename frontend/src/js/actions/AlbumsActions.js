var AppDispatcher = require('../dispatchers/AppDispatcher');

var AlbumsActions = {
    getAlbumsList: function(order) {
        AppDispatcher.handleViewAction({
            actionType: 'SHOW_ALBUMS_LIST',
            order: order
        })
    }
};


module.exports = AlbumsActions;