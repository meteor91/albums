/** @jsx React.DOM */

var React = require('react');
var SimpleAction = require('../actions/SimpleActions');
var SimpleStore = require('../stores/SimpleStore');

var IncreaseButton = React.createClass({
    increaseClick: function() {
        SimpleAction.increaseCounter();
    },
    render: function() {
        return (
            <button onClick={this.increaseClick}>inc</button>
        )
    }
});

var DecreaseButton = React.createClass({
    decreaseClick: function() {
        SimpleAction.decreaseCounter();
    },
    render: function() {
        return (
            <button onClick={this.decreaseClick}>dec</button>
        )
    }
});


var Counter = React.createClass({
    getInitialState: function() {
        return {count: SimpleStore.getCounterVal()};
    },
	componentWillMount: function() {
		SimpleStore.addChangeListener(this._onChange);
	},
	_onChange: function() {
		this.setState({count: SimpleStore.getCounterVal()});
	},

    render: function() {
        return (
            <div>
                <IncreaseButton />
                <span> + {this.state.count} - </span>
                <DecreaseButton />
            </div>
        )
    }
});

module.exports = Counter;