/** @jsx React.DOM */

var React = require('react');
var Modal = require('react-bootstrap').Modal;
var Button = require('react-bootstrap').Button;

var AlbumsStore = require('../stores/AlbumsStore');
var AlbumsActions = require('../actions/AlbumsActions.js');


var AlbumModalForm = React.createClass({
    getInitialState: function () {
        return { showModal: false };
    },
    open: function () {
        this.setState({ showModal: true });
    },
    close: function () {
        this.setState({ showModal: false });
    },
    render: function () {
        return (
            <div>
                <Button
                    bsStyle="primary"
                    bsSize="small"
                    onClick={this.open}>
                    Launch demo modal
                </Button>
                <Modal show={this.state.showModal} onHide={this.close}>
                    <Modal.Header closeButton>
                        <Modal.Title>Modal heading</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h4>Text in a modal</h4>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
});


module.exports = AlbumModalForm;
