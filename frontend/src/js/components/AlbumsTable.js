/** @jsx React.DOM */

var React = require('react');

var AlbumsStore = require('../stores/AlbumsStore');
var AlbumsActions = require('../actions/AlbumsActions.js');

var OrderQueue = {
    queue: [
        ['name', null],
        ['year', null],
        ['performer', null]
    ],
    getOrders: function() {
    	var r = [];
    	this.queue.map(function(item) {
    		if(item[1]==null) return;
            if(item[1]=='desc') r.push('-'+item[0]);
            if(item[1]=='asc') r.push(item[0]);
    	});
    	return r.join(',');
    },
    setOrder: function(order) {
        var newOrder=null, pos=0;
        this.queue.map(function(item, i) {
      		if(item[0]==order) {
      			if(item[1]==null) {
      				item[1]='asc';
      			} else if (item[1]=='asc') {
      				item[1]='desc';
      			} else if(item[1]=='desc') {
      				item[1]=null;
      			}
      			pos=i;
                newOrder = item;
      		}
        });
        if(newOrder!=null) {
            this.queue.splice(pos, 1);
            this.queue.unshift(newOrder);
        }
    }
};

var AlbumsTable = React.createClass({
    getInitialState: function() {
        return {
            albums: AlbumsStore.getAlbums(),
            orderQueue: OrderQueue
        };
    },

	_onChange: function() {
		this.setState({albums: AlbumsStore.getAlbums()});
	},

    componentWillMount: function() {
        AlbumsStore.addChangeListener(this._onChange);
        AlbumsActions.getAlbumsList(this.order);
    },

    order: function(field) {
        var orderQueue = this.state.orderQueue;
        orderQueue.setOrder(field);
        this.setState({orderQueue: orderQueue});
        AlbumsActions.getAlbumsList(orderQueue.getOrders());
    },

    render: function() {
        return (
            <div>
                <table className='table'>
                    <thead>
                        <tr>
                            <th onClick={this.order.bind(this, 'year')}>Год выпуска</th>
                            <th onClick={this.order.bind(this, 'name')}>Название альбома</th>
                            <th onClick={this.order.bind(this, 'performer')}>Название исполнителя</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.albums.map(function(album) {
                            return (
                                <tr key={album.pk}>
                                    <td>{album.year}</td>
                                    <td>{album.name}</td>
                                    <td>{album.performer}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
});

module.exports = AlbumsTable;